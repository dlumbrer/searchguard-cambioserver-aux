#!/bin/bash

# Create structure
mkdir -p releases
mkdir -p releases/searchguard

# Copy dependencies
cp -rf lib/ releases/searchguard/.
cp -rf node_modules/ releases/searchguard/.
cp -rf public/ releases/searchguard/.
cp index.js releases/searchguard/.
cp package.json releases/searchguard/.

# Build zip
cd releases
[ -e searchguard-biter-6.1.0-10-1.zip ] && rm searchguard-biter-6.1.0-10-1.zip
zip -r searchguard-biter-6.1.0-10-1.zip searchguard

# Delete useless folder
rm -rf searchguard